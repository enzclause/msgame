package ezih.minesweeper.console

import java.io.InputStream

import cats.effect.{Blocker, Concurrent, ContextShift, ExitCode, IO, IOApp, Sync, Timer}
import cats.implicits._
import cats.{Applicative, Functor}
import ezih.minesweeper.game.{BoardOps, Bots, GameProps}
import fs2.concurrent.SignallingRef
import fs2.{Stream, io}
import org.jline.terminal.Terminal

import scala.concurrent.duration._

object FS2Tagless_Impl extends IOApp {
  import BoardOps._
  import ConsoleGame._
  import TaglessConsoleGame._

  def getTerminal[F[_]: Sync]: F[Terminal] = Sync[F].delay(ConsoleGame.createTerminal)

  def stupidBlindBotAction[F[_]: Functor](p: Player, gameState: F[GameState]): F[GameCommand] =
    gameState.map(Bots.stupidBlindBot(p, _))

  def userInputStream[F[_]: Sync: ContextShift](is: InputStream): Stream[F, GameCommand] =
    Stream.resource(Blocker[F]).flatMap { blocker: Blocker =>
      io.readInputStream[F](Sync[F].delay(is), chunkSize = 1, blocker, closeAfterUse = true)
        .map(_.toInt)
        .map(i => interpretPlayerInput(i))
    }

  def createGameStream[F[_]: Concurrent: ContextShift: Timer: GameDrawer]: Stream[F, Unit] =
    for {
      terminal       <- Stream.eval(getTerminal[F])
      terminalStream <- Stream.eval(Sync[F].delay(terminal.input()))
      initialState = GameState(board = createAndInit(GameProps(10, 10, mineChance = 20)))
      gStateRef <- Stream.eval(SignallingRef[F, GameState](initialState))
      _         <- Stream.eval(GameDrawer[F].drawGame(initialState))

      botStream: Stream[F, GameCommand] = Stream
        .awakeEvery[F](600.milliseconds)
        .evalMap(_ => gStateRef.get)
        .flatMap(game => Stream.emits(game.bots.map(gStateRef.get -> _).toSeq))
        .parEvalMap[F, GameCommand](5)(game2PlayerId => stupidBlindBotAction[F](game2PlayerId._2, game2PlayerId._1))

      userIStream: Stream[F, GameCommand] = userInputStream(terminalStream)
        .takeWhile(_ != GameCommand.Exit)

      _ <- userIStream
        .mergeHaltL[F, GameCommand](botStream)
        .scan(initialState)(calculateState)
        .evalMap(nextState => gStateRef.set(nextState) *> Applicative[F].pure(nextState))
        .debounce(40.milliseconds) // drop excessive render candidates. FPS limit ?
        .evalMap(nextState => GameDrawer[F].redrawGame(nextState))

    } yield ()

  override def run(args: List[String]): IO[ExitCode] =
    createGameStream[IO].compile.drain
      .as(ExitCode.Success)

}
