package ezih.minesweeper.console

import cats.effect.{ IO, Sync }
import cats.syntax.apply._
import ezih.minesweeper.game.BoardOps.{ flagCell, openCell }
import ezih.minesweeper.game._
import org.jline.terminal.{ Terminal, TerminalBuilder }

import scala.util.Random

object ConsoleGame {
  import AnsiSupport._

  type Player = String
  val HumanPlayer: Player = "Humonoid"

  val defaultBotSupplier: GameState => GameState = game => {
    val botName  = "b_" + game.players.size
    val startPos = Pos(Random.nextInt(game.board.rows), Random.nextInt(game.board.cols))
    game.copy(players = game.players + (botName -> PlayerState(pos = startPos, color = Color.random)),
              bots = game.bots + botName)
  }

  case class PlayerState(player: Player = HumanPlayer, pos: Pos, color: AnsiColor = Color.Red, score: Int = 0)
  case class GameState(board: GBoard,
                       botCreator: GameState => GameState = defaultBotSupplier,
                       bots: Set[Player] = Set.empty,
                       players: Map[Player, PlayerState] = Map(HumanPlayer -> PlayerState(pos = Pos(0, 0))),
                       isEnded: Boolean = false)

  sealed abstract class GameCommand(player: Player = HumanPlayer) {
    def getPlayer: Player = player
  }
  object GameCommand {
    case class Left(player: Player)         extends GameCommand(player)
    case class Right(player: Player)        extends GameCommand(player)
    case class Up(player: Player)           extends GameCommand(player)
    case class Down(player: Player)         extends GameCommand(player)
    case class Open(player: Player)         extends GameCommand(player)
    case class Flag(player: Player)         extends GameCommand(player)
    case object Exit                        extends GameCommand(HumanPlayer)
    case object AddBot                      extends GameCommand(HumanPlayer)
    case class Unrecognized(player: Player) extends GameCommand(player)
  }

  private val IODrawer = TaglessConsoleGame.GameDrawer.ansiTerminalIO

  def drawGame(gameState: GameState): IO[Unit] = IODrawer.drawGame(gameState)

  def redrawGame(gameState: GameState): IO[Unit] = IODrawer.redrawGame(gameState)

  def interpretPlayerInput(charIdx: Int): GameCommand = charIdx match {
    case 13 | 101 => GameCommand.Open(HumanPlayer)
    case 102      => GameCommand.Flag(HumanPlayer)
    case 119      => GameCommand.Up(HumanPlayer)
    case 97       => GameCommand.Left(HumanPlayer)
    case 115      => GameCommand.Down(HumanPlayer)
    case 100      => GameCommand.Right(HumanPlayer)
    case 27       => GameCommand.Exit
    case 98       => GameCommand.AddBot
    case _        => GameCommand.Unrecognized(HumanPlayer)
  }

  def calculateState(gameState: GameState, command: GameCommand): GameState = {
    def updatePos(game: GameState, p: Player)(f: Pos => Pos): GameState = {
      val pState = game.players(p)
      game.copy(players = game.players + (p -> pState.copy(pos = f(pState.pos))))
    }
    def updateStats(newBoard: GBoard, p: Player): GameState = {
      def cellsScore(cell: Cell): Int = cell match {
        case Opened(aroundHint) => aroundHint + 1
        case Flagged(_, false)  => -4
        case Flagged(_, true)   => 2 // cheating?
        case Exploded           => -10
        case _                  => 0
      }
      val playerState = gameState.players(command.getPlayer)
      val oldBoard    = gameState.board
      val newScore = newBoard.cells.collect {
        case (pos, cell) if oldBoard.cells(pos) != cell => cell
      }.foldLeft(playerState.score)(_ + cellsScore(_))

      gameState.copy(
        board = newBoard,
        players = gameState.players + (p -> playerState.copy(score = newScore)),
        isEnded = BoardOps.isEnded(newBoard)
      )
    }

    val pos   = gameState.players(command.getPlayer).pos
    val board = gameState.board
    command match {
      case GameCommand.Left(p) if pos.y > 0               => updatePos(gameState, p)(_.copy(y = pos.y - 1))
      case GameCommand.Right(p) if pos.y < board.cols - 1 => updatePos(gameState, p)(_.copy(y = pos.y + 1))
      case GameCommand.Up(p) if pos.x > 0                 => updatePos(gameState, p)(_.copy(x = pos.x - 1))
      case GameCommand.Down(p) if pos.x < board.rows - 1  => updatePos(gameState, p)(_.copy(x = pos.x + 1))

      case GameCommand.Open(p) => updateStats(openCell(pos, board), p)
      case GameCommand.Flag(p) => updateStats(flagCell(pos, board), p)

      case GameCommand.Exit   => gameState.copy(isEnded = true)
      case GameCommand.AddBot => gameState.botCreator(gameState)
      case _                  => gameState
    }
  }

  def createTerminal: Terminal = {
    val terminal = TerminalBuilder
      .builder()
      .jansi(true)
      .system(true)
      .build()
    terminal.enterRawMode()
    terminal
  }
}

object AnsiSupport {
  val ansiColorReset: String = "\u001b[0m"
  //  val lineReset: String                                = "\u001b[2K"
  val clearLinePostfix: String = "\u001b[0K"
  //  val resetScreen: String                              = "\u001b[2J"
  def ansiCursorUp(times: Int): String                 = s"\u001b[${times}A"
  def highLightWith(str: String, c: AnsiColor): String = s"${c()}$str$ansiColorReset"

  sealed trait AnsiColor { def apply(): String }
  object Color {
    case object Red     extends AnsiColor { def apply: String = "\u001b[41;1m" }
    case object Green   extends AnsiColor { def apply: String = "\u001b[42;1m" }
    case object Magenta extends AnsiColor { def apply: String = "\u001b[45;1m" }
    case object Yellow  extends AnsiColor { def apply: String = "\u001b[43;1m" }

    private val botColors = List(Color.Green, Color.Magenta, Color.Yellow)

    def random = botColors(Random.nextInt(botColors.length))
  }
}

object TaglessConsoleGame {
  import AnsiSupport._
  import ConsoleGame._

  trait GameDrawer[F[_]] {
    def drawGame(gameState: GameState): F[Unit]
    def redrawGame(gameState: GameState): F[Unit]
  }
  object GameDrawer {
    def apply[F[_]](implicit inst: GameDrawer[F]): GameDrawer[F] = inst
    implicit val ansiTerminalIO: GameDrawer[IO]                  = new TerminalGameDrawer[IO]
  }

  class TerminalGameDrawer[F[_]: Sync] extends GameDrawer[F] {
    import cats.syntax.all.{ toFlatMapOps, toFunctorOps }

    val footerRows = 5

    def drawGame(gameState: GameState): F[Unit] =
      for {
        _ <- printLine()
        _ <- printLine(
          "Scores # " + gameState.players
                .map(name2state => s"[${name2state._1}]: ${name2state._2.score}")
                .mkString(clearLinePostfix + "\n/ ")
        )
        _ <- printLine()
        _ <- printGameBoard(gameState)
        _ <- printLine()
        _ <- printLine("[W][A][S][D] - move, [E] - open, [F] - flag, [b] - add bot, [ESC] - exit")
        _ <- printLine()

      } yield ()

    def redrawGame(gameState: GameState): F[Unit] =
      for {
        _ <- carriageReturn(times = gameState.board.rows + gameState.players.size)
        _ <- drawGame(gameState)
      } yield ()

    private def carriageReturn(times: Int): F[Unit] =
      Sync[F].delay(print(ansiCursorUp(times + footerRows) + "\r"))

    private def printGameBoard(game: GameState): F[Unit] = {
      def cellRepr(cell: Cell): String = cell match {
        case Opened(aroundHint) => aroundHint.toString
        case Flagged(_, _)      => "~"
        case Exploded           => "*"
        case Hidden(_, _)       => " "
      }

      def taggedCell(cellPos: Pos, cellStr: String): String =
        game.players.values
          .find(_.pos == cellPos)
          .fold(cellStr) { player =>
            AnsiSupport.highLightWith(cellStr, player.color)
          }

      def printableRow(row: Seq[(Pos, String)]): String =
        "|" + row.map(pos2Cell => (taggedCell _).tupled(pos2Cell)).mkString("|") + "|"

      (for {
        x <- 0 until game.board.rows
        y <- 0 until game.board.cols
      } yield Pos(x, y))
        .map(p => p -> cellRepr(game.board.cells(p)))
        .sliding(game.board.cols, game.board.cols)
        .map(printableRow)
        .foldLeft(Sync[F].unit)(_ *> printLine(_))
    }

    private def printLine(line: String = ""): F[Unit] = Sync[F].delay(println(line + clearLinePostfix))

  }

}
