package ezih.minesweeper.console

import java.util.concurrent.atomic.AtomicReference

import akka.NotUsed
import akka.actor.{ ActorSystem, Cancellable }
import akka.stream.scaladsl.{ Flow, Merge, Sink, Source, StreamConverters }
import akka.stream.{ ActorMaterializer, IOResult, OverflowStrategy }
import ezih.minesweeper.game.{ BoardOps, Bots, GameProps }

import scala.concurrent.Future
import scala.concurrent.duration._

object AkkaS_Impl extends App {
  import BoardOps._
  import ConsoleGame._

  implicit val system           = ActorSystem("app")
  implicit val materializer     = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  def stupidBlindBotAction(p: Player, game: GameState): Future[GameCommand] =
    Future(Bots.stupidBlindBot(p, game))

  def processGameAction: Flow[GameCommand, GameState, NotUsed] =
    Flow[GameCommand]
      .scan(initialState)(calculateState)
      .map(nextState => {
        gStateRef.set(nextState) // single writer?
        nextState
      })

  def drawGameBoard: Flow[GameState, Unit, NotUsed] =
    Flow[GameState].map(nextState => {
      redrawGame(nextState).unsafeRunSync()
    })

  val terminal = createTerminal

  val initialState = GameState(board = createAndInit(GameProps(10, 10, mineChance = 20)))
  val gStateRef    = new AtomicReference[GameState](initialState)

  val userInputSrc: Source[GameCommand, Future[IOResult]] = StreamConverters
    .fromInputStream(() => terminal.input(), chunkSize = 1)
    .map(ui => ui.head.toInt)
    .map(interpretPlayerInput)
    .takeWhile(_ != GameCommand.Exit)

  val botInputSrc: Source[GameCommand, Cancellable] = Source
    .tick(0.millisecond, 600.millisecond, ())
    .map(_ => gStateRef.get())
    .mapConcat(game => game.bots.map(gStateRef.get -> _))
    .mapAsyncUnordered(5)(game2PlayerId => stupidBlindBotAction(game2PlayerId._2, game2PlayerId._1))

  val game = Source
    .combine(userInputSrc, botInputSrc)(Merge(_, eagerComplete = true))
    .via(processGameAction)
    .buffer(1, OverflowStrategy.dropTail) // drop excessive render candidates.
    .throttle(25, 1.second)
    .via(drawGameBoard)

  drawGame(initialState).unsafeRunSync()
  game
    .runWith(Sink.ignore)
    .onComplete(_ => {
      terminal.close()
      system.terminate()
    })

}
