package ezih.minesweeper.console

import cats.effect.{ IO, Resource }
import cats.implicits.catsSyntaxFlatMapOps
import ezih.minesweeper.game._
import org.jline.terminal.Terminal
import org.jline.utils.NonBlockingReader

object IO_Impl extends App {
  import BoardOps._
  import ConsoleGame._

  def createTerminal: IO[Terminal] = IO(ConsoleGame.createTerminal)

  def gameLoop(reader: NonBlockingReader, gameState: GameState): IO[Unit] =
    for {
      charIdx <- IO(reader.read())
      command   = interpretPlayerInput(charIdx)
      nextState = calculateState(gameState, command)
      _ <- redrawGame(nextState)
      _ <- if (nextState.isEnded) IO.unit else gameLoop(reader, nextState)
    } yield {}

  val gameConfig = for {
    terminal <- Resource.fromAutoCloseable(createTerminal)
    reader   <- Resource.fromAutoCloseable(IO(terminal.reader()))
    initialState = GameState(createAndInit(GameProps(10, 10, mineChance = 20)))
  } yield (reader, initialState)

  gameConfig.use {
    case (r, initialState) => drawGame(initialState) >> gameLoop(r, initialState)
  }.unsafeRunSync()

}
