package ezih.minesweeper.console

import java.io.InputStream

import cats.effect.{Blocker, ExitCode, IO, IOApp}
import cats.implicits._
import ezih.minesweeper.game.{BoardOps, Bots, GameProps}
import fs2.concurrent.SignallingRef
import fs2.{Stream, io}
import org.jline.terminal.Terminal

import scala.concurrent.duration._

object FS2_Impl extends IOApp {
  import BoardOps._
  import ConsoleGame._

  def getTerminal: IO[Terminal] = IO(ConsoleGame.createTerminal)

  def stupidBlindBotAction(p: Player, gameState: IO[GameState]): IO[GameCommand] =
    gameState.map(Bots.stupidBlindBot(p, _))

  override def run(args: List[String]): IO[ExitCode] =
    for {
      terminal       <- getTerminal
      terminalStream <- IO(terminal.input())
      initialState = GameState(board = createAndInit(GameProps(10, 10, mineChance = 20)))
      gStateRef <- SignallingRef[IO, GameState](initialState)
      _         <- drawGame(initialState)

      botStream = Stream
        .awakeEvery[IO](600.milliseconds)
        .evalMap(_ => gStateRef.get)
        .flatMap(game => Stream.emits(game.bots.map(gStateRef.get -> _).toSeq))
        .parEvalMapUnordered(5)(game2PlayerId => stupidBlindBotAction(game2PlayerId._2, game2PlayerId._1))

      userIStream = userInputStream(terminalStream)
        .takeWhile(_ != GameCommand.Exit)

      gameStream = userIStream
        .mergeHaltL(botStream)
        .scan(initialState)(calculateState)
        .evalMap(nextState => gStateRef.set(nextState) *> IO.pure(nextState))
        .takeWhile(!_.isEnded)
        .debounce(40.milliseconds) // drop excessive render candidates.
        .evalMap(nextState => redrawGame(nextState))

      exit <- gameStream.compile.drain
        .as(ExitCode.Success)
    } yield exit

  private def userInputStream(is: InputStream): Stream[IO, GameCommand] =
    Stream.resource(Blocker[IO]).flatMap { blocker: Blocker =>
      io.readInputStream[IO](IO(is), chunkSize = 1, blocker, closeAfterUse = true)
        .map(_.toInt)
        .map(i => interpretPlayerInput(i))
    }
}
