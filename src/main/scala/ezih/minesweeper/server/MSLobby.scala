package ezih.minesweeper.server

import java.util.UUID.randomUUID

import akka.actor.{ Actor, ActorRef, Props }
import ezih.minesweeper.game.GameProps
import ezih.minesweeper.server.LobbyActor._
import io.circe.Encoder
import io.circe.generic.semiauto._

case class Game(uid: String = randomUUID().toString)(val name: String,
                                                     val players: List[Player],
                                                     val timestamp: Long = System.currentTimeMillis())

class LobbyActor extends Actor {

  val games = scala.collection.mutable.Map.empty[String, (Game, ActorRef)]

  override def receive: Receive = {
    case LobbyCmd.LocateGame(uid: String) =>
      sender ! games.get(uid).map(_._2)

    case LobbyCmd.ListGames =>
      sender ! games.values.map(g => GameInfo(g._1.uid, g._1.name, g._1.players.size))

    case LobbyCmd.CreateGame(gameName) =>
      val gameActor = context.actorOf(GameActor.props(GameProps(10, 10, mineChance = 10), self))
      val game      = Game()(name = gameName, players = List.empty[Player])
      games += game.uid -> (game -> gameActor)
      sender ! GameInfo(game.uid, game.name, game.players.size)
  }
}

object LobbyActor {
  def props = Props(classOf[LobbyActor])

  sealed trait LobbyCmd
  object LobbyCmd {
    final case class LocateGame(uid: String)  extends LobbyCmd
    final case class ListGames()              extends LobbyCmd
    final case class CreateGame(name: String) extends LobbyCmd
  }

  case class GameInfo(uid: String, name: String, players: Int)
  object GameInfo {
    implicit val gameInfoEncoder: Encoder[GameInfo] = deriveEncoder
  }
}
