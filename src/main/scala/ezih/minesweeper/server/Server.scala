package ezih.minesweeper.server

import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.Credentials.{ Missing, Provided }
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import ezih.minesweeper.server.LobbyActor.GameInfo
import io.circe.syntax._

import scala.concurrent.duration._
import scala.util.{ Failure, Success }

object Server {
  implicit val system           = ActorSystem("app")
  implicit val materializer     = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  implicit val timeout          = Timeout(3.seconds)

  private val gameServer: MSGame = new MSGame()

  def main(args: Array[String]): Unit = {
    println("Starting mine-sweeper server...")

    def lobbyRoute: Route = path("api" / "games") {
//      authenticateBasic(realm = "inmemoryWonderland", playersAuthenticator) { player =>
      get {
        onComplete(gameServer.lobbyActor ? LobbyActor.LobbyCmd.ListGames) {
          case Success(games: List[GameInfo]) =>
            complete(StatusCodes.OK -> games.asJson.toString())
          case Failure(msg) =>
            complete(StatusCodes.InternalServerError -> s"Failed to locate games : $msg")
        }
      } ~
        (post & entity(as[String])) { gameName =>
          onComplete(gameServer.lobbyActor ? LobbyActor.LobbyCmd.CreateGame(gameName)) {
            case Success(game: GameInfo) =>
              complete(StatusCodes.OK -> game.asJson.toString())
            case Failure(msg) =>
              complete(StatusCodes.InternalServerError -> s"Failed to create game : $msg")
          }
        }
//      }
    }

    def apiRoute: Route = path("api" / "joingame") {
//      authenticateBasic(realm = "wonderland", playersAuthenticator) { player =>
      get {
        parameters("name", "gameId") { (name, gameId) =>
          onComplete(gameServer.lobbyActor ? LobbyActor.LobbyCmd.LocateGame(gameId)) {
            case Success(Some(gameRef: ActorRef)) =>
              val player = Player(name, "") //replace with Auth-based player
              handleWebSocketMessages(gameServer.createFlow(player, gameRef))
            case Success(_) =>
              complete(StatusCodes.NotFound -> s"Failed to locate game $gameId")
            case Failure(msg) =>
              complete(StatusCodes.InternalServerError -> s"Failed to locate game $gameId : $msg")
          }
        }
      }
//      }
    }

//    def uiRoute: Route =
//      pathEndOrSingleSlash {
//        getFromResource("web/index.html")
//      } ~ getFromResourceDirectory("web")

    val hostAddress = "localhost"

    Http()
      .bindAndHandle(lobbyRoute ~ apiRoute /*~ uiRoute*/, hostAddress, 8084)
      .map(_ => println(s"Server is running at http://$hostAddress:8084/"))
  }

  def playersAuthenticator: Authenticator[Player] = {
    case pAuth @ Provided(uname) =>
      PlayersRepository
        .find(uname)
        .filter(p => pAuth.verify(p.hash, PlayersRepository.weakSaltedHash))
    case Missing => None
  }

}
