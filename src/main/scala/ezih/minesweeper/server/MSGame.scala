package ezih.minesweeper.server

import akka.actor.{ Actor, ActorLogging, ActorRef, ActorSystem, Props }
import akka.http.scaladsl.model.ws.{ Message, TextMessage }
import akka.stream.scaladsl.{ Flow, Keep, Sink, Source }
import akka.stream.{ Materializer, OverflowStrategy }
import ezih.minesweeper.game.{ Cell, Exploded, Flagged, GBoard, GameProps, Hidden, Opened, Pos }
import ezih.minesweeper.server.GameActor._
import io.circe.generic.semiauto.deriveEncoder
import io.circe.syntax._
import io.circe.{ parser, Decoder, Encoder, HCursor }
import org.reactivestreams.Publisher

import scala.collection.mutable

class MSGame(implicit system: ActorSystem, mat: Materializer) {

  val lobbyActor = system.actorOf(Props(classOf[LobbyActor]))

  def createFlow(p: Player, gameRef: ActorRef): Flow[Message, Message, Any] = {
    val (playerActor: ActorRef, publisher: Publisher[TextMessage.Strict]) =
      Source
        .actorRef[String](16, OverflowStrategy.dropTail)
        .map(TextMessage.Strict)
        .toMat(Sink.asPublisher(fanout = false))(Keep.both)
        .run()

    gameRef ! PlayerJoined(p, playerActor)

    val sink: Sink[Message, Any] = Flow[Message].map {
      case TextMessage.Strict(msg) => parser.decode[GameAction](msg)
      case _                       => Left(new IllegalStateException("Only TextMessage.Strict communication is supported"))
    }.map {
      case Right(gameAction) => gameRef ! PlayerAction(p, gameAction)
      case Left(reason)      => println(reason)
    }.to(Sink.onComplete(_ => gameRef ! PlayerLeft(p, playerActor)))

    Flow.fromSinkAndSource(sink, Source.fromPublisher(publisher))
  }

}

class GameActor(gameProps: GameProps, lobby: ActorRef) extends Actor with ActorLogging {
  val players      = mutable.Map.empty[Player, ActorRef]
  val playersState = mutable.Map.empty[Player, PlayerState].withDefault(p => PlayerState(p.name))

  import ezih.minesweeper.game.BoardOps._
  import cats.implicits._

  var board: GBoard = (create _).andThen(calculateHints)(gameProps)

  def receive: Receive = {

    case PlayerAction(player, action) =>
      def updatedBoardForAction(pos: Pos)(action: (Pos, GBoard) => GBoard): Option[GBoard] =
        for {
          newBoard     <- action(pos, board).some
          updatedCells <- updatedCells(board, newBoard)
          _     = updatePlayerState(player, pos, updatedCells)
          state = gameStateUpdate(updatedCells)
          _     = broadcast(state)
        } yield newBoard

      action match {

        case GameAction.FlagCell(pos) =>
          updatedBoardForAction(pos)(flagCell)
            .foreach(board = _)

        case GameAction.OpenCell(pos) =>
          board = updatedBoardForAction(pos)(openCell)
            .fold(ifEmpty = board)(identity)

        case GameAction.GetBoard =>
          val cState = board.cells.map(pos2Cell => CellState(pos2Cell._1, pos2Cell._2)).toList
          val pState = playersState.values.toList
          players(player) ! GameState(cState, pState).asJson.toString()

        case GameAction.Unrecognized(msg) =>
          println(s"failed to parse $msg")
      }

    case PlayerJoined(p, ref) =>
      players += (p -> ref)

    case PlayerLeft(p, ref) =>
      players -= p

  }

  private def updatePlayerState(player: Player, pos: Pos, updatedCells: Map[Pos, Cell]): Unit = {
    val currentPlayerState = playersState(player)
    val minesExploded = updatedCells.count {
      case (_, Exploded) => true
      case _             => false
    }
    playersState += player -> currentPlayerState.copy(
      lastPos = Some(pos),
      deaths = currentPlayerState.deaths + minesExploded
    )
  }

  private def gameStateUpdate(updatedCells: Map[Pos, Cell]): GameState = {
    val cState = updatedCells.map(pos2Cell => CellState(pos2Cell._1, pos2Cell._2)).toList
    val pState = playersState.values.toList
    val state  = GameState(cState, pState)
    state
  }

  private def updatedCells(board: GBoard, newBoard: GBoard): Option[Map[Pos, Cell]] =
    newBoard.cells.collect {
      case pos2cell @ (pos, cell) if board.cells(pos) != cell => pos2cell
    }.some.filter(_.nonEmpty)

  def isGameEnded(board: GBoard): Boolean =
    isEnded(board)

  def broadcast(msg: GameState): Unit =
    players.values.foreach(_ ! msg.asJson.toString())

}

object GameActor {
  def props(gameProps: GameProps, lobby: ActorRef) = Props(classOf[GameActor], gameProps, lobby)

  sealed trait GameAction
  object GameAction {
    final case class OpenCell(p: Pos)          extends GameAction
    final case class FlagCell(p: Pos)          extends GameAction
    final case object GetBoard                 extends GameAction
    final case class Unrecognized(msg: String) extends GameAction

    implicit val gameActionDecoder: Decoder[GameAction] =
      (hCursor: HCursor) => {
        for {
          command <- hCursor.get[String]("name")
          action <- command.toUpperCase match {
            case "GETBOARD" => Right(GetBoard)
            case "OPENCELL" => hCursor.get[Pos]("payload").map(OpenCell)
            case "FLAGCELL" => hCursor.get[Pos]("payload").map(FlagCell)
            case _          => Right(Unrecognized(command))
          }
        } yield action
      }
  }

  sealed trait PlayersAction
  final case class PlayerJoined(p: Player, ref: ActorRef)      extends PlayersAction
  final case class PlayerLeft(p: Player, ref: ActorRef)        extends PlayersAction
  final case class PlayerAction(p: Player, action: GameAction) extends PlayersAction

  case class PlayerState(name: String, lastPos: Option[Pos] = None, deaths: Int = 0)
  object PlayerState {
    implicit val playerStateEncoder: Encoder.AsObject[PlayerState] = deriveEncoder[PlayerState]
  }
  case class CellState(p: Pos, state: String)
  object CellState {
    implicit val cellStateEncoder: Encoder.AsObject[CellState] = deriveEncoder[CellState]
    def apply(p: Pos, c: Cell): CellState =
      c match {
        case Opened(aroundHint) => CellState(p, aroundHint.toString)
        case Flagged(_, _)      => CellState(p, "!")
        case Exploded           => CellState(p, "*")
        case Hidden(_, _)       => CellState(p, "?")
      }

  }
  case class GameState(cells: List[CellState], players: List[PlayerState])
  object GameState {
    implicit val gameStateEncoder: Encoder.AsObject[GameState] = deriveEncoder[GameState]
  }

}
