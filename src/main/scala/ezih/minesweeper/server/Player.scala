package ezih.minesweeper.server

import java.math.BigInteger
import java.security.MessageDigest

case class Player(name: String, hash: String)

object PlayersRepository {
  private val salt    = "hardcodedSalt"
  private val players = scala.collection.mutable.ListBuffer.empty[Player]

  register("admin", "admin")
  register("player", "player")

  def find(name: String): Option[Player] =
    players.find(_.name == name)

  def register(name: String, pwdHash: String): Option[Player] =
    find(name) match {
      case None =>
        val newPlayer = Player(name, weakSaltedHash(pwdHash))
        players += newPlayer
        Some(newPlayer)
      case Some(_) => None
    }

  def weakSaltedHash(s: String): String = {
    val digest = MessageDigest.getInstance("MD5").digest((salt + s).getBytes)
    new BigInteger(1, digest).toString(16)
  }
}
