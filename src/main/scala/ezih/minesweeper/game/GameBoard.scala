package ezih.minesweeper.game

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

import scala.annotation.tailrec
import scala.util.Random

//								            	   [open]     +---------------+   [open]
//							          	 +--------------->+  Opened(hint) +----------+
//			+---------------+    |                +---------------+          |
//			|     Hidden    +----+                                           v
//			+---------------+    |     [open]     +---------------+    {conditionally open neighbours}
//             ^             +--------------->+   Exploded    +
//             |               |              +---------------+
//             |         [flag]|
//             |               v
//             |   [unFlag]  +-------------+
//             +---------<---+   Flagged   +
//                           +-------------+
sealed trait Cell
case class Hidden(aroundHint: Int = 0, isMine: Boolean = false)  extends Cell
case class Flagged(aroundHint: Int = 0, isMine: Boolean = false) extends Cell
case class Opened(aroundHint: Int = 0)                           extends Cell
case object Exploded                                             extends Cell

case class Pos(x: Int, y: Int) {
  def up: Pos     = this.copy(x - 1, y)
  def down: Pos   = this.copy(x + 1, y)
  def right: Pos  = this.copy(x, y + 1)
  def left: Pos   = this.copy(x, y - 1)
  def r_up: Pos   = this.copy(x - 1, y + 1)
  def l_up: Pos   = this.copy(x - 1, y - 1)
  def r_down: Pos = this.copy(x + 1, y + 1)
  def l_down: Pos = this.copy(x + 1, y - 1)

  def adjacent: List[Pos] = left :: right :: up :: down :: r_up :: r_down :: l_up :: l_down :: Nil
}
object Pos {
  implicit val posDecoder: Decoder[Pos] = deriveDecoder[Pos]
  implicit val posEncoder: Encoder.AsObject[Pos] = deriveEncoder[Pos]

  def fromDim(rows: Int, cols: Int): Seq[Pos] =
    for {
      x <- 0 until rows
      y <- 0 until cols
    } yield Pos(x, y)
}

case class GameProps(rows: Int, cols: Int, mineChance: Int)

case class GBoard(rows: Int, cols: Int, cells: Map[Pos, Cell])

object BoardOps {

  def create(props: GameProps): GBoard = {
    def seedValue(mineChance: Int): Cell = Hidden(isMine = Random.nextInt(100) < mineChance)
    val cells                            = Pos.fromDim(props.rows, props.cols).map(_ -> seedValue(props.mineChance))
    GBoard(props.rows, props.cols, cells.toMap)
  }

  def calculateHints(b: GBoard): GBoard =
    b.copy(cells = b.cells.map {
      case (pos, cell: Hidden) => pos -> cell.copy(aroundHint = minesAround(pos, b))
      case pair                => pair
    })

  def createAndInit: GameProps => GBoard = (create _).andThen(calculateHints)

  def neighbours(p: Pos, board: GBoard): List[(Pos, Cell)] =
    p.adjacent
      .filter(isPosDefined(_, board))
      .map(pos => pos -> board.cells(pos))

  def isPosDefined(p: Pos, b: GBoard): Boolean =
    p.x >= 0 && p.y >= 0 && p.y < b.cols && p.x < b.rows

  def mapBoardsCellAt(b: GBoard, p: Pos)(f: Cell => Cell): GBoard =
    b.cells.get(p) match {
      case Some(cell) => b.copy(cells = b.cells + (p -> f(cell))) //Lenses maybe?
      case None       => b
    }

  def boardWithRevealedCell(pos: Pos, board: GBoard): GBoard = mapBoardsCellAt(board, pos) {
    case Hidden(_, isMine) if isMine => Exploded
    case Hidden(hint, false)         => Opened(hint)
    case cell                        => cell
  }

  def flagCell(pos: Pos, board: GBoard): GBoard = mapBoardsCellAt(board, pos) {
    case Hidden(hint, isMine)  => Flagged(hint, isMine)
    case Flagged(hint, isMine) => Hidden(hint, isMine)
    case cell                  => cell
  }

  def openCell(p: Pos, b: GBoard): GBoard = {
    @tailrec def openCells(toVisit: Set[Pos], visited: Set[Pos], b: GBoard): GBoard =
      toVisit.headOption match { //NB orderDependent
        case Some(pos) =>
          val toVisitTail = toVisit - pos
          b.cells(pos) match {
            case Opened(_) | Exploded        => openCells(toVisitTail, visited + pos, b) // skip
            case Hidden(_, isMine) if isMine => boardWithRevealedCell(pos, b) // you die ?
            case Flagged(_, _)               => openCells(toVisitTail, visited + pos, flagCell(pos, b)) // flip flag
            case Hidden(hint, _) if hint > 0 => openCells(toVisitTail, visited + pos, boardWithRevealedCell(pos, b))
            case Hidden(hint, _) if hint == 0 => // open safe cell and neighbours
              val neighbours = pos.adjacent.filter(isPosDefined(_, b)).toSet
              openCells(toVisit = toVisitTail ++ neighbours - pos,
                        visited = visited + pos,
                        boardWithRevealedCell(pos, b))
          }
        case _ => b
      }

    //open cells unvisited neighbours if cell hint matches with already flagged & exploded neighbours
    def openNeighboursWhenHintConditionFulfilled(hint: Int): GBoard = {
      def explodedOrFlaggedCount(nCells: List[(Pos, Cell)]): Int =
        nCells.count {
          case (_, Exploded)      => true
          case (_, Flagged(_, _)) => true
          case _                  => false
        }

      def collectUnvisitedPos(cells: List[(Pos, Cell)]): Set[Pos] =
        cells.collect { case (pos, Hidden(_, _)) => pos }.toSet

      neighbours(p, b) match {
        case nCells: List[(Pos, Cell)] if explodedOrFlaggedCount(nCells) == hint =>
          openCells(toVisit = collectUnvisitedPos(nCells), visited = Set(p), b)
        case _ => b
      }
    }

    b.cells.get(p) match {
      case Some(Opened(hint)) if hint > 0 => openNeighboursWhenHintConditionFulfilled(hint)
      case Some(_)                        => openCells(toVisit = Set(p), visited = Set.empty, b)
      case None                           => b
    }

  }

  def isEnded(board: GBoard): Boolean =
    !board.cells.values.exists {
      case Hidden(_, _)                  => true
      case Flagged(_, isMine) if !isMine => true
      case _                             => false
    }

  private def minesAround(p: Pos, board: GBoard): Int =
    neighbours(p, board).count {
      case (pos, Hidden(_, isMine)) if isMine  => true
      case (pos, Flagged(_, isMine)) if isMine => true
      case _                                   => false
    }

}
