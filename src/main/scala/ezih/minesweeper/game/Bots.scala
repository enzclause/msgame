package ezih.minesweeper.game

import cats.Eval
import cats.data.State
import ezih.minesweeper.console.ConsoleGame.{GameCommand, GameState, Player}
import ezih.minesweeper.game.BoardOps.neighbours

import scala.annotation.tailrec
import scala.util.Random

object Bots {

  def stupidBlindBot(p: Player, game: GameState): GameCommand = {
    val options = GameCommand.Up(p) :: GameCommand.Down(p) :: GameCommand.Left(p) :: GameCommand.Right(p) ::
          GameCommand.Up(p) :: GameCommand.Down(p) :: GameCommand.Left(p) :: GameCommand.Right(p) ::
          GameCommand.Up(p) :: GameCommand.Down(p) :: GameCommand.Left(p) :: GameCommand.Right(p) ::
          GameCommand.Open(p) :: Nil
    options(Random.nextInt(options.length))
  }

  case class BotContext(botID: Player, target: Pos, action: GameCommand, board: GBoard) {
    def makeStep(game: GameState): (BotContext, GameCommand) = {
      val maybePosCandidate = findFirst(game.board, findSafeCell) orElse findFirst(game.board, findMineCell)
      ???
    }

    private def findFirst(board: GBoard, f: ((Pos, Cell), GBoard) => Option[Pos]): Option[Pos] =
      board.cells.foldLeft(Option.empty[Pos])(_ orElse f(_, board))

    private def findSafeCell(p2c: (Pos, Cell), board: GBoard): Option[Pos] = p2c match {
      case (pos, Opened(aroundHint)) =>
        val cneighbours      = neighbours(pos, board)
        val markedNeighbours = cneighbours.collect { case (pos, Flagged(_, _) | Exploded) => pos }
        if (markedNeighbours.size == aroundHint)
          cneighbours.collectFirst { case (pos, Hidden(_, _)) => pos } else None
      case _ => None
    }

    private def findMineCell(p2c: (Pos, Cell), board: GBoard): Option[Pos] = p2c match {
      case (pos, Opened(aroundHint)) =>
        val hiddenNeighbours = neighbours(pos, board).collect { case (pos, Hidden(_, _)) => pos }
        if (hiddenNeighbours.size == aroundHint) hiddenNeighbours.headOption else None
      case _ => None
    }
  }

  // foldM ?
  private def foldWhile[A, T](xs: List[T])(acc: A)(predicate: A => Boolean)(f: (A, T) => A): A = {
    @tailrec def fold(seq: List[T], acc: A): A = seq match {
      case head :: tail if predicate(acc) => fold(tail, f(acc, head))
      case _                                  => acc
    }
    fold(xs, acc)
  }

}
