# MineSweeper games
A set of small minesweeper-game implementations.  

### Overview 
##### Project was started with a goal of herding some cats & bending some light.
* Core [GameBoard](src/main/scala/ezih/minesweeper/game/GameBoard.scala) logic was created.
* Akka-based multilayer game [Server](src/main/scala/ezih/minesweeper/server/Server.scala) was added
    * Game-Lobby - uses some http verbs for creating, listing & joining games. LobbyActor guards list of games.
    * Games use WebSockets for communication (JSON & Circe). GameActor is responsible for game state manipulation.
    * A simple playable HTML UI was created. It's made in old-fashioned-jquery-dinosaur style hence not included in repo.
* At some point Akka-based version was abandoned in favor of small console-UI implementation:
    * Started with a simple impure procedural implementation and gradually reworked it into [IO-based](src/main/scala/ezih/minesweeper/console/IO_Impl.scala) implementation (single-player).
    * Added support for bots in next implementations (only random-stepping behaviour impl. for now).
    * Had some fun implementing game [with FS2](src/main/scala/ezih/minesweeper/console/FS2_Impl.scala). 
    * Had less fun reimplementing it [with Akka-Streams](src/main/scala/ezih/minesweeper/console/AkkaS_Impl.scala).
    * Reworked FS2 version with [tagless encoding](src/main/scala/ezih/minesweeper/console/FS2Tagless_Impl.scala).
    * Started implementing Cats-Effects "only" version (with Fibers and explicit context-shifting).
    Got some working intermediate version yet I didn't master IO-Cancellation so far. Not included in this repo.

Implementations were derived from an undisciplined sandbox-style home project - hence the pile of dependencies and 
some degree of coupling & code-duplication. There are some rough edges I'm aware of - for instance Game win/end logic is badly attended.

### Prerequisites
* JDK8
* sbt
* Terminal?

### Running the game
Project contains multiple main-classes. Some straightforward ways to run desired game-implementations are:

* creating uber-jar with `sbt assembly` and specifying entry-point with
    * `java -cp target/scala-2.12/minesweepers-assembly-0.5.0.jar ezih.minesweeper.console.IO_Impl` 
    * `java -cp target/scala-2.12/minesweepers-assembly-0.5.0.jar ezih.minesweeper.console.FS2_Impl`
    * `java -cp target/scala-2.12/minesweepers-assembly-0.5.0.jar ezih.minesweeper.console.AkkaS_Impl` 
    * `java -cp target/scala-2.12/minesweepers-assembly-0.5.0.jar ezih.minesweeper.console.FS2Tagless_Impl` 
* running `sbt run` and selecting desired main-class (not the stablest option)

## Built With
* sbt 

## Acknowledgments
* winmine.exe
* John A De Goes
* CoRecursive