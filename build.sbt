import Dependencies._

ThisBuild / scalaVersion     := "2.12.8"
ThisBuild / version          := "0.5.0"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "minesweepers",
    libraryDependencies += scalaTest % Test
  )

val catsVersion = "2.0.0"
val catsCore = "org.typelevel" %% "cats-core" % catsVersion
val catsFree = "org.typelevel" %% "cats-free" % catsVersion
//val catsMtl = "org.typelevel" %% "cats-mtl-core" % "0.2.1"

val simulacrum = "com.github.mpilquist" %% "simulacrum" % "0.11.0"
val macroParadise = compilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)
val kindProjector = compilerPlugin("org.spire-math" %% "kind-projector" % "0.9.4")
val resetAllAttrs = "org.scalamacros" %% "resetallattrs" % "1.0.0"

val circeVersion = "0.12.3"

val monocleVersion = "2.0.0"


val specs2Version = "3.6" // use the version used by discipline
val specs2Core  = "org.specs2" %% "specs2-core" % specs2Version
val specs2Scalacheck = "org.specs2" %% "specs2-scalacheck" % specs2Version
val scalacheck = "org.scalacheck" %% "scalacheck" % "1.12.4"


libraryDependencies ++= Seq(
  catsCore,
//  "org.fusesource.jansi" % "jansi" % "1.18",
  "org.jline" % "jline" % "3.13.1",
  "org.jline" % "jline-reader" % "3.13.1",
  "org.jline" % "jline-terminal-jansi" % "3.13.1",

//  "org.typelevel" %% "alleycats-core" % "2.0.0",
//  "org.scala-lang.modules" %% "scala-xml" % "1.2.0",

  "com.typesafe.akka" %% "akka-http" % "10.1.8",
  "com.typesafe.akka" %% "akka-stream" % "2.5.19",

  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "de.heikoseeberger" %% "akka-http-circe" % "1.29.1",

  "co.fs2" %% "fs2-core" % "2.1.0",
  "co.fs2" %% "fs2-io" % "2.1.0",

//  "com.github.julien-truffaut" %%  "monocle-core"  % monocleVersion,
//  "com.github.julien-truffaut" %%  "monocle-macro" % monocleVersion,

  kindProjector
)

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-Ypartial-unification",
  "-feature",
  "-language:_"
)

