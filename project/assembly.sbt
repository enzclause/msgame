
resolvers += Resolver.url(
  "sbt-plugin-releases",
  new URL("https://dl.bintray.com/sbt/sbt-plugin-releases/")
)(Resolver.ivyStylePatterns)

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.10")